<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="AAErm">

    <title>Тест POST</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" >

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js"></script>

</head>

<body class="bg-light">

<div class="container">
    <div class="py-5 text-center header">
        <h4 class="mb-3">Тестим POST, GET передачи используя стили boostrap</h4>
        &nbsp;
        <a href="https://youtu.be/HcfHBgUTn7I">
            <img class="d-block mx-auto mb-4" src="https://s1.anekdoty.ru/uploads/images/funny/open/khuzhe-nekuda-no-ya-starayus-open.jpg"
                 alt="" width="400" height="200">
        </a>
    </div>

    <div id="inputs" class="mx-auto">
        <div class="py-5 text-center add">
            <form  method="post">
                <h4 class="mb-3">Образец формы</h4>
                <input type="text" class="form-control mx-auto" name="row1" placeholder="Введите строку" value="<?php if( isset($_REQUEST['row1']) ) { echo "Ты написал: ".$_REQUEST['row1']; } ?>" required>
                <hr class="mb-4">
                    <button type="submit" class="btn btn-primary btn-lg btn-block submit-btn mx-auto submit-btn1">Понеслась_POST</button>
            </form>
            <form method="get">
                <input type="hidden" name="const" value="Text">
                <textarea name="name" cols="50" rows="10">
                    <?php echo $_GET['const'] ?>
                </textarea>
                <hr class="mb-4">
                    <button type="submit" class="btn btn-primary btn-lg btn-block submit-btn mx-auto submit-btn1">Понеслась_GET</button>
            </form>>
        </div>
    </div>



    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; AAErm</p>
    </footer>
</div>

</body>
</html>